# feathers-api

> 

## About

api-framework based on feathersjs

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Install your dependencies

    ```
    npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers CLI:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```

## Contributing

Diretso pull request! ahaha! joke2 pman gud ni ako study2 pa